import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "../src/screens/Home";
import LoB from "../src/screens/LoB"
import Notifications from "../src/screens/Notifications"
import Profile from "../src/screens/Profile";
import Login from "../src/screens/Login"
import About from "../src/screens/About"
import Settings from "../src/screens/Settings"

const Stack = createStackNavigator();

const MainStackNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="About" component={About} />
        </Stack.Navigator>
    );
}

const LoBNavigator = () => {
    return (
        <Stack.Navigator >
            <Stack.Screen name="LoB" component={LoB} />
        </Stack.Navigator>
    );
}

const NotificationNavigator = () => {
    return (
        <Stack.Navigator >
            <Stack.Screen name="Notifications" component={Notifications} />
        </Stack.Navigator>
    );
}

const ProfileNavigator = () => {
    return (
        <Stack.Navigator >
            <Stack.Screen name="Profile" component={Profile} />
        </Stack.Navigator>
    );
}

const SettingsNavigator = () => {
    return (
        <Stack.Navigator >
            <Stack.Screen name="Settings" component={Settings} />
        </Stack.Navigator>
    );
}

const LoginNavigator = () => {
    return (
        <Stack.Navigator headerMode="none">
            <Stack.Screen name="Login" component={Login} />
        </Stack.Navigator>
    );
}
export { MainStackNavigator, LoBNavigator, NotificationNavigator, ProfileNavigator, SettingsNavigator, LoginNavigator };