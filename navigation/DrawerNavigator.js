import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import TabNavigator from "./TabNavigator";
import { SettingsNavigator, LoginNavigator } from "./StackNavigator";
import { connect } from 'react-redux';


const Drawer = createDrawerNavigator();

const DrawerNavigator = (props) => {
    return (
        <Drawer.Navigator>
            {props.issLoggedIn ? (<>
                <Drawer.Screen name="Home" component={TabNavigator} />
                <Drawer.Screen name="Settings" component={SettingsNavigator} />
            </>) :
                <Drawer.Screen name="Login" component={LoginNavigator} />
            }
        </Drawer.Navigator>
    );
}


const mapStateToProps = (state) => ({
    issLoggedIn: state.loginReducer.issLoggedIn,
});

export default connect(mapStateToProps)(DrawerNavigator);
// export default DrawerNavigator;