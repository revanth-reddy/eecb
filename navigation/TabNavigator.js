import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { MainStackNavigator, LoBNavigator, NotificationNavigator, ProfileNavigator } from "./StackNavigator";

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Home" component={MainStackNavigator} />
            <Tab.Screen name="LoBNavigator" component={LoBNavigator} />
            <Tab.Screen name="NotificationNavigator" component={NotificationNavigator} />
            <Tab.Screen name="ProfileNavigator" component={ProfileNavigator} />
        </Tab.Navigator>
    );
};

export default BottomTabNavigator;