import React from "react";
import { Provider } from 'react-redux';
import { store } from './src/redux/store'
import { NavigationContainer } from "@react-navigation/native";
// import { MainStackNavigator } from "./navigation/StackNavigator";
// import BottomTabNavigator from "./navigation/TabNavigator";
import DrawerNavigator from "./navigation/DrawerNavigator";

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <DrawerNavigator />
      </NavigationContainer>
    </Provider>
  );
}
export default App;
