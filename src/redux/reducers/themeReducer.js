const defaultState = {
    theme: 'default',
};

const types = {
    THEME_SET: 'THEME_SET'
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case types.THEME_SET:
            if (action.payload)
                return { ...state, theme: 'dark' };
            else
                return { ...state, theme: 'default' };
        default:
            return state;
    }
};

export const actions = {
    setTheme: (theme) => ({ type: types.THEME_SET, payload: theme, })
}
