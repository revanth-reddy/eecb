const defaultState = {
    issLoggedIn: false,
};

const types = {
    IS_LOG_IN: 'IS_LOG_IN'
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case types.IS_LOG_IN:
            if (action.payload)
                return { ...state, issLoggedIn: true };
        default:
            return state;
    }
};

export const actions = {
    setLogin: (val) => ({ type: types.IS_LOG_IN, payload: val, })
}
