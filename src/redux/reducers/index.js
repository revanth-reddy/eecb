import { combineReducers } from 'redux'
import themeReducer from './themeReducer'
import loginReducer from './loginReducer'
const allReducers = combineReducers({
    themeReducer,
    loginReducer,
})

export default allReducers;