import React from 'react';
import { createStore } from 'redux';
import AllReducers from './reducers'

export const store = createStore(AllReducers)