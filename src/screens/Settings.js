import React, { Component } from 'react'
import { View, Button, Text, StyleSheet, TouchableOpacity } from "react-native";
import Icon from 'react-native-ionicons';
import { scale } from '../res/responsiveScaling';

export default class Settings extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: "Settings",
        headerStyle: {
            backgroundColor: '#1a4f84',
        },
        headerTintColor: "white",
        headerLeft: () => (
            <TouchableOpacity style={{ marginHorizontal: scale(10) }} onPress={() => navigation.openDrawer()}>
                <Icon name="list" color="white" />
            </TouchableOpacity>
        )


    })

    render() {
        return (
            <View style={styles.center}>
                <Text>This is the Settings screen</Text>
                <Button
                    title="Go to Home Screen"
                    onPress={() => this.props.navigation.navigate("Home")} // We added an onPress event which would navigate to the About screen
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
    },
});