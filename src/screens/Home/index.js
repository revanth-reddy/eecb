import React, { Component } from 'react'
import { View, Button, Text, StyleSheet, TouchableOpacity } from "react-native";
import { connect } from 'react-redux';

class Home extends Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Text>Home</Text>
                <Button title="Go to About Screen" onPress={() => this.props.navigation.navigate("About")} />
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    theme: state.themeReducer.theme,
});

export default connect(mapStateToProps)(Home);