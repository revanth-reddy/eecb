import { StyleSheet } from "react-native";
import { scale, verticalScale } from "../../res/responsiveScaling"

export const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: 'white',
        height: '100%'
    },
    topView: {
        backgroundColor: '#82a0c8',
        paddingHorizontal: scale(34),
        justifyContent: "center",
        alignItems: "center",
        borderBottomLeftRadius: scale(34),
        borderBottomRightRadius: scale(34)
    },
    logo: {
        marginVertical: scale(24),
        height: verticalScale(150),
        resizeMode: 'contain'
    },
    logoTextDetailed: {
        marginBottom: scale(36),
        color: 'white',
        fontFamily: "Poppins",
        fontSize: 18,
        fontWeight: "bold",
        lineHeight: scale(25)
    },
    logoTextShort: {
        marginBottom: scale(30),
        color: 'white',
        fontFamily: "Poppins",
        fontSize: 85,
        fontWeight: "bold",
        lineHeight: scale(119)
    },
    ButtonsView: {
        marginVertical: verticalScale(45),
        paddingHorizontal: scale(34),
        justifyContent: "center",
        alignItems: "center",
        flexDirection: 'column',
    },
    loginButtonsView: {
        marginVertical: verticalScale(32),
        paddingHorizontal: scale(34),
        justifyContent: "center",
        alignItems: "center",
        flexDirection: 'column',
    },
    indButton: {
        height: verticalScale(100),
        justifyContent: 'center',
        alignItems: "center",
        width: '100%',
        borderRadius: scale(25),
        borderWidth: scale(0.25),
        borderColor: '#ececec',
        backgroundColor: 'white',
        elevation: 2,
    },
    facButton: {
        marginTop: verticalScale(18),
        height: verticalScale(100),
        justifyContent: 'center',
        alignItems: "center",
        width: '100%',
        borderRadius: scale(25),
        borderWidth: scale(0.25),
        borderColor: '#ececec',
        backgroundColor: 'white',
        elevation: 2,
    },
    buttonText: {
        color: '#3082cc',
        lineHeight: 20,
        fontFamily: "Poppins",
        fontSize: 14,
        fontWeight: "500",
        fontStyle: "normal",
    },
    policyContainer: { alignItems: "center", marginBottom: verticalScale(17) },
    line: {
        fontFamily: "Poppins",
        fontSize: scale(8),
        textAlign: "left",
        color: "#898a8f"
    },
    hyperLink: {
        fontFamily: "Poppins",
        fontSize: scale(8),
        color: "#3082cc"
    },
    username: {
        height: verticalScale(47),
        width: '100%',
        borderWidth: scale(0.25),
        borderRadius: scale(25),
        borderColor: '#ececec',
        backgroundColor: 'white',
        elevation: 2,
        paddingHorizontal: scale(15),
        lineHeight: verticalScale(20),
        fontFamily: "Poppins",
        fontSize: scale(14),
        color: "#707070",
    },
    password: {
        marginTop: verticalScale(11),
        height: verticalScale(47),
        width: '100%',
        borderWidth: scale(0.25),
        borderRadius: scale(25),
        borderColor: '#ececec',
        backgroundColor: 'white',
        elevation: 2,
        paddingHorizontal: scale(15),
        lineHeight: verticalScale(20),
        fontFamily: "Poppins",
        fontSize: scale(14),
        color: "#707070",
    },
    signInButton: {
        marginTop: verticalScale(10),
        width: scale(160),
        borderWidth: scale(0.25),
        borderRadius: scale(25),
        borderColor: '#ececec',
        backgroundColor: 'white',
        elevation: 2,
        height: verticalScale(38),
        justifyContent: "center"
    },
    signInText: {
        lineHeight: verticalScale(20),
        fontFamily: "Poppins",
        fontSize: scale(14),
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "center",
        color: "#3082cc"
    }
});
