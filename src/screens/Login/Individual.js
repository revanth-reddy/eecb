import React from "react";
import { View, Text, Image, TouchableOpacity, TextInput, ScrollView } from "react-native";
import { styles } from './styles'

const logo = require('../../assets/images/logo/logo.png')
const Individual = () => {
    return (
        <ScrollView style={styles.mainContainer} showsVerticalScrollIndicator={false} scrollEnabled={false}>
            <View style={styles.topView}>
                <Image source={logo} style={styles.logo} />
                <Text style={styles.logoTextShort}>ECEB</Text>
            </View>

            <View style={styles.loginButtonsView}>
                <TextInput style={styles.username} placeholder={'Email'} textContentType={'emailAddress'} />
                <TextInput style={styles.password} placeholder={'Password'} secureTextEntry />
                <TouchableOpacity style={styles.signInButton}>
                    <Text style={styles.signInText}>Sign In</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.policyContainer}>
                <Text style={styles.line}>
                    By continuing, you agree to our{' '}
                    <Text style={styles.hyperLink} onPress={() => console.log('privacy policy')}>
                        Privacy Policies
                                </Text>
                </Text>
                <Text style={styles.line}>
                    <Text style={styles.hyperLink} onPress={() => console.log('Data use Policies')}>
                        Data use Policies
                                </Text>
                                 , including our{' '}
                    <Text style={styles.hyperLink} onPress={() => console.log('Cookie use')}>
                        Cookie use
                                </Text>
                </Text>
            </View>
        </ScrollView>
    );
};


export default Individual;