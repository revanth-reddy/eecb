import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from "react-native";
import { connect } from 'react-redux';
import Individual from './Individual'
import Facility from './Facility'
import { styles } from './styles'

const logo = require('../../assets/images/logo/logo.png')
class Login extends Component {
    constructor() {
        super();
        this.state = {
            currPage: '',
        }
    }

    goTo = (page) => {
        this.setState({ currPage: page })
    }

    render() {
        const { currPage } = this.state
        switch (currPage) {
            case 'individual':
                return <Individual />
            case 'facility':
                return <Facility />
            default:
                return (
                    <View style={styles.mainContainer}>
                        <View style={styles.topView}>
                            <Image source={logo} style={styles.logo} />
                            <Text style={styles.logoTextDetailed}>Essential Care for Every Baby</Text>
                        </View>

                        <View style={styles.ButtonsView}>
                            <TouchableOpacity style={styles.indButton} onPress={() => this.goTo('individual')}>
                                <Text style={styles.buttonText}>Individual</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.facButton} onPress={() => this.goTo('facility')}>
                                <Text style={styles.buttonText}>Facility</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.policyContainer}>
                            <Text style={styles.line}>
                                By continuing, you agree to our{' '}
                                <Text style={styles.hyperLink} onPress={() => console.log('privacy policy')}>
                                    Privacy Policies
                                </Text>
                            </Text>
                            <Text style={styles.line}>
                                <Text style={styles.hyperLink} onPress={() => console.log('Data use Policies')}>
                                    Data use Policies
                                </Text>
                                 , including our{' '}
                                <Text style={styles.hyperLink} onPress={() => console.log('Cookie use')}>
                                    Cookie use
                                </Text>
                            </Text>
                        </View>
                    </View>
                )
        }
    }
}

const mapStateToProps = (state) => ({
    theme: state.themeReducer.theme,
});

export default connect(mapStateToProps)(Login);