import React, { Component } from 'react'
import { View, Button, Text, StyleSheet, TouchableOpacity } from "react-native";
import { connect } from 'react-redux';

class Notifications extends Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Text>Notifications</Text>
                <Button title="Go to Home Screen" onPress={() => this.props.navigation.navigate("Home")} />
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    theme: state.themeReducer.theme,
});

export default connect(mapStateToProps)(Notifications);