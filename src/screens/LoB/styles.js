import { StyleSheet } from "react-native";
import { scale, verticalScale } from "../../res/responsiveScaling"
import { getThemeColor } from "../../res/themes/getThemeColor"

export const getStyles = (theme) => StyleSheet.create({
    menuIcon: {
        marginHorizontal: scale(10)
    },
    headerStyle: {
        backgroundColor: 'violet'
    },
    body: {
        // flex: 1,
        // justifyContent: "center",
        // alignItems: "center",
        // textAlign: "center",
        backgroundColor: getThemeColor('white', theme)
    },
    text: {
        color: getThemeColor('black', theme)
    }
});
